use std::str::FromStr;

use chrono::NaiveDateTime;
use sea_orm_migration::sea_orm::prelude::{Uuid};
use sea_orm_migration::sea_orm::{entity::*, query::*};
use sea_orm_migration::prelude::*;
use entity::{role, user, concentrator, user_concentrator};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        let role_superuser = role::ActiveModel {
            name: Set("Superuser".to_owned()),
            ..Default::default()
        }
        .insert(db)
        .await?;

        let role_owner = role::ActiveModel {
            name: Set("Owner".to_owned()),
            ..Default::default()
        }
        .insert(db)
        .await?;
        
        let user_ovalek = user::ActiveModel {
            id: Set(Uuid::from_str("958f473f-53c0-4bab-93b2-3ed9ae2fce9b").unwrap()),
            role_id: Set(role_superuser.id),
            email: Set("oldrich.valek@gmail.com".to_owned()),
            name: Set("ovalek".to_owned()),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:47", "%Y-%m-%d %H:%M:%S").unwrap()),
            last_login: Set(None),
            password: Set(Some("$2y$12$cFgKf7VkdtjAcEsSKg8Qbe82Dp9AYtF3kDLPYzmj1T7LDcbXdK7Ye".to_owned())),
            data: Set(None),
        }.insert(db)
        .await?;

        let user_admin = user::ActiveModel {
            id: Set(Uuid::from_str("0a9e3092-6f55-49f2-a6e6-e3a634a75a34").unwrap()),
            role_id: Set(role_superuser.id),
            email: Set("admin@nekde.cz".to_owned()),
            name: Set("Admin".to_owned()),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:48", "%Y-%m-%d %H:%M:%S").unwrap()),
            last_login: Set(None),
            password: Set(Some("$2y$12$Nu1GrDkTlObqOnRSGTD3CO2kyk.RNXoFdw2Yj7P9YLFlg3VMGhSF6".to_owned())), // adminadmin
            data: Set(None),
        }.insert(db)
        .await?;

        let user_owner = user::ActiveModel {
            id: Set(Uuid::from_str("631fefae-474b-444a-9a90-88d56e7755cd").unwrap()),
            role_id: Set(role_owner.id),
            email: Set("owner@nekde.cz".to_owned()),
            name: Set("Owner".to_owned()),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:49", "%Y-%m-%d %H:%M:%S").unwrap()),
            last_login: Set(None),
            password: Set(Some("$2y$12$oB1R.vA.Fn/yrzB9.sLAGuv2LefMS4fHShMgHJ5.vAbx.1L1afj3y".to_owned())), // ownerowner
            data: Set(None),
        }.insert(db)
        .await?;

        let user_test = user::ActiveModel {
            id: Set(Uuid::from_str("a434d58b-2d8a-4af9-8c29-727b12d312e8").unwrap()),
            role_id: Set(role_owner.id),
            email: Set("test@nekde.cz".to_owned()),
            name: Set("Test".to_owned()),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:50", "%Y-%m-%d %H:%M:%S").unwrap()),
            last_login: Set(None),
            password: Set(Some("$2y$12$nZ0Xp087D/g6Mkjx/MGoY.OMv5IukGFzCdzBJ3OLxVcS44Oj8v5LW".to_owned())), // testtest
            data: Set(None),
        }.insert(db)
        .await?;

        let concentrator_sth = concentrator::ActiveModel {
            id: Set(Uuid::from_str("18175d24-428b-4043-9e91-be996ba51b22").unwrap()),
            name: Set("Instalace Sth".to_owned()),
            state: Set(concentrator::ConcentratorState::New),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
            last_connection: Set(None),
            requests_count: Set(0),
            private_id: Set(Uuid::from_str("34c5e889-78ba-41af-a2d1-88328463ef81").unwrap()),
            data: Set(None),
        }.insert(db)
        .await?;
     
        let concentrator_1 = concentrator::ActiveModel {
            id: Set(Uuid::from_str("0e6eea14-1b67-437c-a337-434fb18d6dc1").unwrap()),
            name: Set("Lokální instalace 1".to_owned()),
            state: Set(concentrator::ConcentratorState::New),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:52", "%Y-%m-%d %H:%M:%S").unwrap()),
            last_connection: Set(None),
            requests_count: Set(0),
            private_id: Set(Uuid::from_str("ce42219c-8227-480f-96f1-8fd4bf77a7ad").unwrap()),
            data: Set(None),
        }.insert(db)
        .await?;

        let concentrator_2 = concentrator::ActiveModel {
            id: Set(Uuid::from_str("d71d3b99-f504-4bff-bad6-c11bd1e07fdc").unwrap()),
            name: Set("Instalace 2".to_owned()),
            state: Set(concentrator::ConcentratorState::New),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:53", "%Y-%m-%d %H:%M:%S").unwrap()),
            last_connection: Set(None),
            requests_count: Set(0),
            private_id: Set(Uuid::from_str("8cec33dc-6c12-4c40-9ffc-5606a9d1b85c").unwrap()),
            data: Set(None),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_ovalek.id),
            concentrator_id: Set(concentrator_1.id),
            is_owner: Set(true),
            position: Set(0),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_ovalek.id),
            concentrator_id: Set(concentrator_sth.id),
            is_owner: Set(true),
            position: Set(1),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_admin.id),
            concentrator_id: Set(concentrator_1.id),
            is_owner: Set(true),
            position: Set(0),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_owner.id),
            concentrator_id: Set(concentrator_1.id),
            is_owner: Set(true),
            position: Set(0),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_test.id),
            concentrator_id: Set(concentrator_1.id),
            is_owner: Set(true),
            position: Set(0),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:52", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_test.id),
            concentrator_id: Set(concentrator_2.id),
            is_owner: Set(true),
            position: Set(1),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:52", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        concentrator::Entity::delete_many()
            .filter(concentrator::Column::Id.is_in([Uuid::from_str("18175d24-428b-4043-9e91-be996ba51b22").unwrap(), Uuid::from_str("0e6eea14-1b67-437c-a337-434fb18d6dc1").unwrap(), Uuid::from_str("d71d3b99-f504-4bff-bad6-c11bd1e07fdc").unwrap()]))
            .exec(db)
            .await?;

        user::Entity::delete_many()
            .filter(user::Column::Email.is_in(["oldrich.valek@gmail.com", "admin@nekde.cz", "owner@nekde.cz", "test@nekde.cz"]))
            .exec(db)
            .await?;

        role::Entity::delete_many()
            .filter(role::Column::Name.eq("Superuser").or(role::Column::Name.eq("Owner")))
            .exec(db)
            .await?;

        Ok(())
    }
}
