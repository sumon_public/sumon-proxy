use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Role::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Role::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Role::Name).string().not_null().unique_key())
                    .to_owned(),
            ).await?;

        manager
            .create_table(
                Table::create()
                    .table(User::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(User::Id)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(User::RoleId).integer().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-user-role_id")
                            .from(User::Table, User::RoleId)
                            .to(Role::Table, Role::Id)
                            .on_delete(ForeignKeyAction::Restrict)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .col(ColumnDef::new(User::Email).string().not_null().unique_key())
                    .col(ColumnDef::new(User::Name).string().not_null())
                    .col(ColumnDef::new(User::Created).timestamp().not_null().default(SimpleExpr::Keyword(Keyword::CurrentTimestamp)))
                    .col(ColumnDef::new(User::LastLogin).timestamp().null())
                    .col(ColumnDef::new(User::Password).string().null())
                    .col(ColumnDef::new(User::Data).json().null())
                    .to_owned(),
            )
            .await?;

        manager.create_index(sea_query::Index::create().name("ui-user-email")
            .table(User::Table)
            .col(User::Email).unique().to_owned()
        ).await?;

        manager
            .create_table(
                Table::create()
                    .table(Oauth::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Oauth::Id)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Oauth::UserId).uuid().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-oauth-user_id")
                            .from(Oauth::Table, Oauth::UserId)
                            .to(User::Table, User::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .col(ColumnDef::new(Oauth::Type).integer().not_null())
                    .col(ColumnDef::new(Oauth::Username).string().null())
                    .col(ColumnDef::new(Oauth::ClientId).string().null())
                    .col(ColumnDef::new(Oauth::Created).timestamp().not_null().default(SimpleExpr::Keyword(Keyword::CurrentTimestamp)))
                    .col(ColumnDef::new(Oauth::LastLogin).timestamp().null())
                    .col(ColumnDef::new(Oauth::Data).json().null())
                    .to_owned(),
            )
            .await?;
        manager.create_index(sea_query::Index::create().name("ui-oauth-client_id-type")
            .table(Oauth::Table)
            .col(Oauth::ClientId)
            .col(Oauth::Type)
            .unique().to_owned()
        ).await?;
        manager.create_index(sea_query::Index::create().name("i-oauth-username-type")
            .table(Oauth::Table)
            .col(Oauth::Username)
            .col(Oauth::Type)
            .to_owned()
        ).await?;

        manager
            .create_table(
                Table::create()
                    .table(Concentrator::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Concentrator::Id)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Concentrator::Name).string().not_null())
                    .col(ColumnDef::new(Concentrator::State).string().not_null().default("N"))
                    .col(ColumnDef::new(Concentrator::Created).timestamp().not_null().default(SimpleExpr::Keyword(Keyword::CurrentTimestamp)))
                    .col(ColumnDef::new(Concentrator::LastConnection).timestamp().null())
                    .col(ColumnDef::new(Concentrator::RequestsCount).big_integer().not_null().default(0))
                    .col(ColumnDef::new(Concentrator::PrivateId).uuid().not_null())
                    .col(ColumnDef::new(Concentrator::Data).json().null())
                    .to_owned(),
            )
            .await?;
        manager.create_index(sea_query::Index::create().name("i-concentrator-state")
            .table(Concentrator::Table)
            .col(Concentrator::State).to_owned()
        ).await?;

        manager
            .create_table(
                Table::create()
                    .table(UserConcentrator::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(UserConcentrator::UserId).uuid().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-user-concentrator-user_id")
                            .from(UserConcentrator::Table, UserConcentrator::UserId)
                            .to(User::Table, User::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .col(ColumnDef::new(UserConcentrator::ConcentratorId).uuid().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-user-concentrator-concentrator_id")
                            .from(UserConcentrator::Table, UserConcentrator::ConcentratorId)
                            .to(Concentrator::Table, Concentrator::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .primary_key(sea_query::Index::create().name("pi-user-concentrator")
                        .table(UserConcentrator::Table)
                        .col(UserConcentrator::UserId)
                        .col(UserConcentrator::ConcentratorId)
                        .primary()
                    )
                    .col(ColumnDef::new(UserConcentrator::IsOwner).boolean().not_null().default(true))
                    .col(ColumnDef::new(UserConcentrator::Position).integer().not_null().default(0))
                    .col(ColumnDef::new(UserConcentrator::Created).timestamp().not_null().default(SimpleExpr::Keyword(Keyword::CurrentTimestamp)))
                    .to_owned(),
            )
            .await?;
    
        Ok(()) // All good!
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(UserConcentrator::Table).to_owned())
            .await?;

        manager.drop_index(sea_query::Index::drop().name("i-concentrator-state").table(Concentrator::Table).to_owned()).await?;
        manager
            .drop_table(Table::drop().table(Concentrator::Table).to_owned())
            .await?;

        manager.drop_index(sea_query::Index::drop().name("i-oauth-username-type").table(Oauth::Table).to_owned()).await?;
        manager.drop_index(sea_query::Index::drop().name("ui-oauth-client_id-type").table(Oauth::Table).to_owned()).await?;
        manager
            .drop_table(Table::drop().table(Oauth::Table).to_owned())
            .await?;

        manager.drop_index(sea_query::Index::drop().name("ui-user-email").table(User::Table).to_owned()).await?;
        manager
            .drop_table(Table::drop().table(User::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Role::Table).to_owned())
            .await?;

        Ok(()) // All good!
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
enum Role {
    Table,
    Id,
    Name,
}

#[derive(Iden)]
enum User {
    Table,
    Id,
    RoleId,
    Email,
    Name,
    Created,
    LastLogin,
    Password,
    Data,
}

#[derive(Iden)]
enum Oauth {
    Table,
    Id,
    UserId,
    Type,
    Username,
    ClientId,
    Created,
    LastLogin,
    Data,
}

#[derive(Iden)]
enum Concentrator {
    Table,
    Id,
    Name,
    State,
    Created,
    LastConnection,
    RequestsCount,
    PrivateId,
    Data,
}

#[derive(Iden)]
enum UserConcentrator {
    Table,
    UserId,
    ConcentratorId,
    IsOwner,
    Position,
    Created,
}