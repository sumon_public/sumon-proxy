use std::str::FromStr;

use chrono::NaiveDateTime;
use entity::oauth::OauthType;
use sea_orm_migration::sea_orm::prelude::{Uuid};
use sea_orm_migration::sea_orm::{entity::*, query::*};
use sea_orm_migration::prelude::*;
use entity::{oauth, concentrator, user_concentrator};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        let user_ovalek = Uuid::from_str("958f473f-53c0-4bab-93b2-3ed9ae2fce9b").unwrap();

        oauth::ActiveModel {
            id: Set(Uuid::from_str("5cfccebe-6c9c-4d6a-9861-0128850aae51").unwrap()),
            user_id: Set(user_ovalek),
            r#type: Set(OauthType::Google),
            username: Set(Some("oldrich.valek@gmail.com".to_string())),
            client_id: Set(None),
            created: Set(NaiveDateTime::parse_from_str("2023-05-09 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
            last_login: Set(None),
            data: Set(None),
        }.insert(db)
        .await?;

        let user_admin = Uuid::from_str("0a9e3092-6f55-49f2-a6e6-e3a634a75a34").unwrap();

        let user_owner = Uuid::from_str("631fefae-474b-444a-9a90-88d56e7755cd").unwrap();

        let user_test = Uuid::from_str("a434d58b-2d8a-4af9-8c29-727b12d312e8").unwrap();
    
        let concentrator_2 = Uuid::from_str("d71d3b99-f504-4bff-bad6-c11bd1e07fdc").unwrap();

        let concentrator_hk = concentrator::ActiveModel {
            id: Set(Uuid::from_str("91e6ee15-5b1d-41ad-a475-dc40465aa31f").unwrap()),
            name: Set("Lokální instalace HK".to_owned()),
            state: Set(concentrator::ConcentratorState::New),
            created: Set(NaiveDateTime::parse_from_str("2023-05-09 21:00:00", "%Y-%m-%d %H:%M:%S").unwrap()),
            last_connection: Set(None),
            requests_count: Set(0),
            private_id: Set(Uuid::from_str("04c3523d-3180-4c4d-8cf7-3ed64cebbc2e").unwrap()),
            data: Set(None),
        }.insert(db)
        .await?;

        // assign concentrator_2 to all remaining users
        user_concentrator::ActiveModel {
            user_id: Set(user_ovalek),
            concentrator_id: Set(concentrator_2),
            is_owner: Set(true),
            position: Set(0),
            created: Set(NaiveDateTime::parse_from_str("2023-05-09 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_admin),
            concentrator_id: Set(concentrator_2),
            is_owner: Set(true),
            position: Set(1),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_owner),
            concentrator_id: Set(concentrator_2),
            is_owner: Set(true),
            position: Set(1),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        // test@nekde.cz already has this relation
        // user_concentrator::ActiveModel {
        //     user_id: Set(user_test),
        //     concentrator_id: Set(concentrator_2),
        //     is_owner: Set(true),
        //     position: Set(1),
        //     created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        // }.insert(db)
        // .await?;

        // assign concentrator_hk to all users
        user_concentrator::ActiveModel {
            user_id: Set(user_ovalek),
            concentrator_id: Set(concentrator_hk.id),
            is_owner: Set(true),
            position: Set(0),
            created: Set(NaiveDateTime::parse_from_str("2023-05-09 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_admin),
            concentrator_id: Set(concentrator_hk.id),
            is_owner: Set(true),
            position: Set(1),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_owner),
            concentrator_id: Set(concentrator_hk.id),
            is_owner: Set(true),
            position: Set(1),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        user_concentrator::ActiveModel {
            user_id: Set(user_test),
            concentrator_id: Set(concentrator_hk.id),
            is_owner: Set(true),
            position: Set(1),
            created: Set(NaiveDateTime::parse_from_str("2023-05-01 03:39:51", "%Y-%m-%d %H:%M:%S").unwrap()),
        }.insert(db)
        .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        user_concentrator::Entity::delete_many()
            .filter(user_concentrator::Column::ConcentratorId.eq(Uuid::from_str("d71d3b99-f504-4bff-bad6-c11bd1e07fdc").unwrap())
                .and(user_concentrator::Column::UserId.is_in([
                    Uuid::from_str("958f473f-53c0-4bab-93b2-3ed9ae2fce9b").unwrap(),
                    Uuid::from_str("0a9e3092-6f55-49f2-a6e6-e3a634a75a34").unwrap(),
                    Uuid::from_str("631fefae-474b-444a-9a90-88d56e7755cd").unwrap(),
                ]))
            ).exec(db).await?;

        concentrator::Entity::delete_many()
            .filter(concentrator::Column::Id.is_in([Uuid::from_str("91e6ee15-5b1d-41ad-a475-dc40465aa31f").unwrap()]))
            .exec(db)
            .await?;

        oauth::Entity::delete_many()
            .filter(oauth::Column::Id.is_in([Uuid::from_str("5cfccebe-6c9c-4d6a-9861-0128850aae51").unwrap()]))
            .exec(db)
            .await?;
        Ok(())
    }
}
