pub use sea_orm_migration::prelude::*;

mod m20230430_000001_create_tables;
mod m20230501_033948_initial_data;
mod m20230509_180039_another_data;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20230430_000001_create_tables::Migration),
            Box::new(m20230501_033948_initial_data::Migration),
            Box::new(m20230509_180039_another_data::Migration),
        ]
    }
}
