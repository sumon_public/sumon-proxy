use base64::{Engine as _, engine::general_purpose};
use jwt_compact::alg::Hs256Key;
use rand::rngs::OsRng;

fn main() {
    println!("Create a new key for the `HS512` JWT algorithm:");
    let key = Hs256Key::generate(&mut OsRng);
    let base64_key: String = general_purpose::STANDARD_NO_PAD.encode(key.into_inner());
    println!("{:?}", base64_key);
}