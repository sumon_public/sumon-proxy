# Entity

https://www.sea-ql.org/SeaORM/docs/generate-entity/entity-structure/

https://www.sea-ql.org/SeaORM/docs/generate-entity/entity-structure/#active-model-behavior

- Generate entities using db schema (from proxy dir)
    ```sh
    sea-orm-cli generate entity -o entity/src -l
    ```