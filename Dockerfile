FROM rust:1.78.0-bookworm@sha256:5907e96b0293eb53bcc8f09b4883d71449808af289862950ede9a0e3cca44ff5 as builder
# FROM rust:1.75.0-bookworm as builder
################
# https://levelup.gitconnected.com/create-an-optimized-rust-alpine-docker-image-1940db638a6c
##### Builder

# Set the working directory
WORKDIR /usr/src/sumon-proxy

# my copy Cargo config and entity & migration + cargo fetch to cache dependencies
COPY --link sumon-common /usr/src/sumon-common
COPY --link entity /usr/src/sumon-proxy/entity
COPY --link migration /usr/src/sumon-proxy/migration
COPY --link Cargo.lock Cargo.toml /usr/src/sumon-proxy/
COPY --link dummy.rs .
RUN sed -i 's#src/main.rs#dummy.rs#' Cargo.toml
RUN cargo build --release --bin sumon-proxy
RUN sed -i 's#dummy.rs#src/main.rs#' Cargo.toml

#RUN cargo fetch

# Copy in the rest of the sources
COPY --link . /usr/src/sumon-proxy/

## Touch main.rs to prevent cached release build
RUN touch /usr/src/sumon-proxy/src/main.rs

# This is the actual application build.
RUN cargo build --release --bin sumon-proxy

# fetch frontend
# first add # syntax = docker/dockerfile:experimental
# enable necessary variables via AUTO_DEVOPS_BUILD_IMAGE_FORWARDED_CI_VARIABLES=READ_ONLY_PRIVATE_TOKEN
# mount variables https://docs.gitlab.com/ee/topics/autodevops/customize.html#forward-cicd-variables-to-the-build-environment
#RUN --mount=type=secret,id=auto-devops-build-secrets . /run/secrets/auto-devops-build-secrets && $COMMAND
# RUN curl --location --output frontend-artifacts.zip --header "PRIVATE-TOKEN: ${READ_ONLY_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/44523725/jobs/artifacts/main/download?file_type=archive&job=frontend-build" \
#     && mkdir -p target/final-build && unzip frontend-artifacts.zip -d target/final-build 
#     # should create folder frontend/build inside target/final-build

################
##### Runtime
# FROM rust:1.75.0-slim-bookworm AS runtime
FROM debian:bookworm-slim@sha256:804194b909ef23fb995d9412c9378fb3505fe2427b70f3cc425339e48a828fca AS runtime

# install openssl
RUN apt-get update && apt-get install -y --no-install-recommends openssl ca-certificates \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Copy application binary from builder image
COPY --from=builder /usr/src/sumon-proxy/target/release/sumon-proxy /usr/local/bin

WORKDIR /app

# copy downloaded frontend
# COPY --from=builder /usr/src/sumon-proxy/target/final-build/frontend frontend

COPY --link target/final-build/frontend frontend
COPY --link .env .

EXPOSE 8040

# Run the application
CMD ["/usr/local/bin/sumon-proxy"]