# Sumon-proxy

## Run dev proxy backend
```shell
cargo run
```
API runs on (http://localhost:8040/_/)

DEV frontend by default listens on (http://localhost:3000) and automatically proxies API `_/` path to (http://localhost:8040/_/).

### Use production frontend
To use production frontend, copy it's built `dist` folder into `sumon-proxy/frontend/dist`. It should be accessible as (http://localhost:8040). For google login is necessary to change `LISTEN_PORT` in `.env` to `3000`. Then use (http://localhost:3000) to access production frontend and (http://localhost:3000/_/) to access proxy API.

## Generate own PRIVATE_KEY
PRIVATE_KEY is necessary for JWT authentication to encrypt cookie tokens.
```shell
cargo run --bin keygen
```

## Run production proxy docker image locally
Make sure database is running by using `docker compose up -d` in `sumon/sumon-dev` repository. Then
```shell
docker run -p127.0.0.1:3000:3000 --network sumon_default -eDATABASE_URL="postgresql://sumon_proxy:sumon_proxy@sumon-proxy-postgres:5432/sumon_proxy" -eLISTEN_IP=0.0.0.0 -eLISTEN_PORT=3000 -eRUST_BACKTRACE=full some-private-registry/sumon/sumon-proxy:latest
```

Open (http://localhost:3000)