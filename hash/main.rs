use bcrypt::{hash, DEFAULT_COST};
use std::io;

fn main() {
    println!("Fill password that you want to hash:");

    let mut password = String::new();
    match io::stdin().read_line(&mut password) {
        Ok(_) => {}
        Err(_) => return,
    };

    // Remove trailing newline character
    password.pop();

    let maybe_hash = hash(password, DEFAULT_COST);
    match maybe_hash {
        Ok(hash) => println!("{}", hash),
        Err(e) => println!("Failed to create hash: {}", e),
    };
}
