use actix::{fut, ActorContext};
use crate::websocket::ClientResponseMessage;
use crate::websocket::messages::{Disconnect, Connect, WsConnMessage};
use crate::websocket::server::WsServer;
use actix::{Actor, Addr, Running, StreamHandler, WrapFuture, ActorFutureExt, ContextFutureSpawner};
use actix::{AsyncContext, Handler};
use actix_web_actors::ws;
use actix_web_actors::ws::Message::Text;
use std::time::{Duration, Instant};
use uuid::Uuid;

const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(30);

pub struct WsConn {
    server_addr: Addr<WsServer>,
    hb: Instant,
    id: Uuid,
}

impl WsConn {
    pub fn new(client_id: Uuid, server: Addr<WsServer>) -> WsConn {
        WsConn {
            id: client_id,
            hb: Instant::now(),
            server_addr: server,
        }
    }
}

impl Actor for WsConn {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.hb(ctx);

        let addr = ctx.address();
        self.server_addr
            .send(Connect {
                addr: addr.recipient(),
                self_id: self.id,
            })
            .into_actor(self)
            .then(|res, _, ctx| {
                match res {
                    Ok(_res) => (),
                    _ => ctx.stop(),
                }
                fut::ready(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        self.server_addr.do_send(Disconnect { id: self.id, manual: false });
        Running::Stop
    }
}

impl WsConn {
    fn hb(&self, ctx: &mut ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                println!("Disconnecting failed heartbeat");
                ctx.stop();
                return;
            }

            ctx.ping(b"PING");
        });
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for WsConn {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        match msg {
            Ok(ws::Message::Ping(msg)) => {
                self.hb = Instant::now();
                ctx.pong(&msg);
            }
            Ok(ws::Message::Pong(_)) => {
                self.hb = Instant::now();
            }
            Ok(ws::Message::Binary(bin)) => ctx.binary(bin),
            Ok(ws::Message::Close(reason)) => {
                ctx.close(reason);
                ctx.stop();
            }
            Ok(ws::Message::Continuation(_)) => {
                ctx.stop();
            }
            Ok(ws::Message::Nop) => (),
            // Ok(Text(s)) => self.server_addr.do_send(ClientActorMessage {
            //     id: self.id,
            //     msg: s
            // }),
            Ok(Text(s)) => {
                // println!("WsConn -> Server msg: {}", s);
                self.server_addr.do_send(ClientResponseMessage{client_id: self.id, msg: Ok(s.to_string())});
            },
            Err(e) => {
                println!("WsConn Err: {}", e.to_string());
                self.server_addr.do_send(ClientResponseMessage{client_id: self.id, msg: Err(format!("WsConn Err: {}", e.to_string()))});
            },
        }
    }
}

impl Handler<WsConnMessage> for WsConn {
    type Result = ();

    fn handle(&mut self, msg: WsConnMessage, ctx: &mut Self::Context) {
        match msg {
            WsConnMessage::WsMessage(ws_msg) => {
                ctx.text(ws_msg.0);
            }
            WsConnMessage::CloseSelf(close_msg) => {
                println!("WS stopped the connection because new one is being established");
                ctx.close(close_msg.reason);
                ctx.stop();
            }
        }
    }
}