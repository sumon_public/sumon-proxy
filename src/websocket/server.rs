use crate::websocket::{Util, messages::{Connect, Disconnect, WsConnMessage, WsMessage, RequestMessage, ProxyRequestInternal, ProxyResponse, ClientResponseMessage}, CloseSelf};
use actix_web_actors::ws::CloseCode;
use actix::{prelude::{Actor, Context, Handler, Recipient}, AsyncContext};
use entity::concentrator::ConcentratorState;
use std::pin::Pin;
use std::time::{Duration, Instant};
use std::{collections::HashMap, future::Future};
use uuid::Uuid;
use tokio::sync::oneshot;
use tokio;

type Socket = Recipient<WsConnMessage>;

pub struct MessageChannel {
    tx: oneshot::Sender<Result<ProxyResponse, String>>,
    timestamp: std::time::Instant,
}

pub struct WsServer {
    sessions: HashMap<Uuid, Socket>,
    messages: HashMap<Uuid, HashMap<String, MessageChannel>>,
    util: Util,
}

impl WsServer {
    pub fn new(db: sea_orm::DatabaseConnection) -> WsServer {
        WsServer {
            sessions: HashMap::new(),
            messages: HashMap::new(),
            util: Util::new(db.clone()),
        }
    }
}

// TODO: remove
impl WsServer {
    fn send_message(&self, message: &str, id_to: &Uuid) {
        if let Some(socket_recipient) = self.sessions.get(id_to) {
            let _ = socket_recipient.do_send(WsConnMessage::WsMessage(WsMessage(message.to_owned())));
        } else {
            println!("attempting to send message but couldn't find client id.");
        }
    }
}

impl WsServer {
    fn clear_old_messages(&mut self) {
        let now = Instant::now();
        for messages in self.messages.values_mut() {
            messages.retain(|_hash, channel| {
                let age = now.duration_since(channel.timestamp);
                age < Duration::from_secs(60)
            });
        }
    }

    fn refresh_states(&self) {
        let ids: Vec<_> = self.sessions.keys().cloned().collect();
        self.util.refresh_multiple_concentrators_state(ids);
    }
}

impl Actor for WsServer {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        // ctx.set_mailbox_capacity(32);
        ctx.run_interval(std::time::Duration::from_secs(30), |act, _ctx| {
            act.clear_old_messages();
        });
        ctx.run_interval(std::time::Duration::from_secs(5), |act, _ctx| {
            act.refresh_states();
        });
    }
}

/// Handler for Disconnect message.
impl Handler<Disconnect> for WsServer {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _: &mut Context<Self>) {
        if msg.manual {
            if let Some(socket_recipient) = self.sessions.get(&msg.id){
                println!("WS manual disconnect: {}", msg.id);
                let _ = socket_recipient.do_send(WsConnMessage::CloseSelf(CloseSelf { reason: Some(CloseCode::Normal.into()) }));
            }
        }
        if self.sessions.remove(&msg.id).is_some() {
            // maybe remove pending messages?
        }

        self.util.set_concentrator_state(msg.id, ConcentratorState::Offline);
    }
}

impl Handler<Connect> for WsServer {
    type Result = ();

    fn handle(&mut self, msg: Connect, _: &mut Context<Self>) -> Self::Result {
        // store the address
        self.sessions.insert(
            msg.self_id,
            msg.addr,
        );

        self.util.set_concentrator_state(msg.self_id, ConcentratorState::Online);

        // send self your new uuid
        self.send_message(&format!("your id is {}", msg.self_id), &msg.self_id);
    }
}


impl Handler<RequestMessage> for WsServer {
    type Result = Pin<Box<dyn Future<Output = Result<ProxyResponse, String>>>>;

    fn handle(&mut self, msg: RequestMessage, _ctx: &mut Self::Context) -> Self::Result {
        println!("handle RequestMessage for {}", msg.client_id);
        self.util.increment_concentrator_requests(msg.client_id.clone());

        let (tx, rx) = oneshot::channel();
        let timestamp = std::time::Instant::now();
        let msg_channel = MessageChannel { tx, timestamp };

        let preq_internal: ProxyRequestInternal = msg.request.into();

        if let Some(msg_hashmap) = self.messages.get_mut(&msg.client_id) {
            msg_hashmap.insert(preq_internal.hash.clone(), msg_channel); // hash map exists, add message
        } else {
            let mut msg_hashmap: HashMap<String, MessageChannel> = HashMap::new(); // create new hash map
            msg_hashmap.insert(preq_internal.hash.clone(), msg_channel); // add message
            self.messages.insert(msg.client_id.clone(), msg_hashmap); // insert hash map to messages
        }

        if let Some(socket_recipient) = self.sessions.get(&msg.client_id) {
            let serialized_proxy_request = match serde_json::to_string(&preq_internal) {
                Ok(str) => str,
                Err(_) => {
                    let pri = preq_internal;
                    println!("Couldn't serialize given proxy request {} {} {} {}", pri.method, pri.resource, pri.body, pri.hash);
                    "{}".to_string()
                }
            };
            let _ = socket_recipient.do_send(WsConnMessage::WsMessage(WsMessage(serialized_proxy_request.to_owned())));
        } else {
            let fut = async move {
                Err("Couldn't find client id".to_owned())
            };

            return Box::pin(fut);
        }

        let fut = async move {
            rx.await
                .map_err(|e| format!("Response channel closed: {}", e))
                .and_then(|inner_result| inner_result)
        };

        Box::pin(fut)
    }
}

impl Handler<ClientResponseMessage> for WsServer {
    type Result = ();

    fn handle(&mut self, msg: ClientResponseMessage, _ctx: &mut Self::Context) -> Self::Result {
        let raw_response: String = match msg.msg {
            Ok(raw) => raw,
            Err(_) => return // on error -> ignore message
        };
        let proxy_response: ProxyResponse = match serde_json::from_str(&raw_response) {
            Ok(resp) => resp,
            Err(_) => return // invalid json response -> ignore message
        };
        if let Some(client_messages) = self.messages.get_mut(&msg.client_id) {
            if let Some(msg_channel) = client_messages.remove(&proxy_response.hash) {
                // found matching response, send it through oneshot channel back to proxy controller
                let _ = msg_channel.tx.send(Ok(proxy_response));
            }
        }
    }
}