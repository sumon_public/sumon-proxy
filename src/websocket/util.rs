use chrono::Utc;
use entity::concentrator::{Entity as Concentrator, self, ConcentratorState};
use migration::Expr;
use sea_orm::{entity::*, DatabaseConnection, ConnectionTrait, EntityTrait, QueryFilter, Statement, DatabaseBackend};
use uuid::Uuid;


pub struct Util {
    db: DatabaseConnection,
}

impl Util {
    pub fn new(db: DatabaseConnection) -> Util {
        Util {
            db
        }
    }
}

impl Util {
    /// change concentrator state in a fire & forget manner
    pub fn set_concentrator_state(&self, id: Uuid, new_state: ConcentratorState) {
        let db = self.db.clone();
        tokio::task::spawn(async move {
            match Concentrator::find_by_id(id).one(&db).await {
                Ok(maybe_row) => {
                    match maybe_row {
                        Some(row) => {
                            let mut concentrator: concentrator::ActiveModel = row.into();
                            concentrator.state = Set(new_state.to_owned());
                            if new_state.eq(&ConcentratorState::Online) {
                                let utc_now = Utc::now();
                                concentrator.last_connection = Set(Some(utc_now.naive_utc()));
                            }
                            let _ = concentrator.update(&db).await;
                        },
                        None => {},
                    }
                },
                Err(_) => {},
            };
        });
    }

    /// update the state of multiple concentrators in a fire & forget manner
    pub fn refresh_multiple_concentrators_state(&self, online_ids: Vec<Uuid>) {
        let db = self.db.clone();
        tokio::task::spawn(async move {
            // set offline
            let _ = Concentrator::update_many()
                .col_expr(concentrator::Column::State, Expr::value(ConcentratorState::Offline))
                .filter(
                    concentrator::Column::State.eq(ConcentratorState::Online)
                    .and(concentrator::Column::Id.is_not_in(online_ids.clone()))
                )
                .exec(&db)
                .await;

            // set online
            let _ = Concentrator::update_many()
                .col_expr(concentrator::Column::State, Expr::value(ConcentratorState::Online))
                .filter(
                    concentrator::Column::Id.is_in(online_ids)
                    .and(concentrator::Column::State.is_in(vec![ConcentratorState::Offline, ConcentratorState::New]))
                )
                .exec(&db)
                .await;
        });
    }

    pub fn increment_concentrator_requests(&self, id: Uuid) {
        let db = self.db.clone();
        tokio::task::spawn(async move {
            // Increment using raw query so it is closer to atomic.
            // In a standard orm update could other threads increment the value resulting in lower final number.
            let _ = db.execute(Statement::from_sql_and_values(
                DatabaseBackend::Postgres,
                "UPDATE concentrator SET requests_count = requests_count + 1 WHERE id = $1",
                vec![id.into()]
            )).await;
        });
    }
}