use actix::prelude::{Message, Recipient};
use actix_web_actors::ws::CloseReason;
use serde::{Serialize, Deserialize};
use uuid::Uuid;
use std::hash::{Hash, Hasher};

// Wrapped message to distinguish between websocket message and internal CloseSelf message
#[derive(Message)]
#[rtype(result = "()")]
pub enum WsConnMessage {
    WsMessage(WsMessage),
    CloseSelf(CloseSelf),
}

//WsConn responds to this to pipe it through to the actual client
#[derive(Message)]
#[rtype(result = "()")]
pub struct WsMessage(pub String);

//WsConn sends this to the lobby to say "put me in please"
#[derive(Message)]
#[rtype(result = "()")]
pub struct Connect {
    pub addr: Recipient<WsConnMessage>,
    pub self_id: Uuid,
}

//WsConn sends this to a lobby to say "take me out please"
#[derive(Message)]
#[rtype(result = "()")]
pub struct Disconnect {
    pub id: Uuid,
    pub manual: bool,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct CloseSelf {
    pub reason: Option<CloseReason>,
}

//server app sends this to WsServer actor that passes it to client and waits for a response to this specific message
#[derive(Message)]
#[rtype(result = "Result<ProxyResponse, String>")]
pub struct RequestMessage {
    pub client_id: Uuid,
    pub request: ProxyRequest,
}

#[derive(Serialize, Deserialize)]
pub struct ProxyRequest {
    pub method: String,
    pub resource: String,
    pub body: String,
}

#[derive(Serialize)]
pub struct ProxyRequestInternal {
    pub method: String,
    pub resource: String,
    pub body: String,
    pub hash: String,
}

#[derive(Deserialize)]
pub struct ProxyResponse {
    pub hash: String,
    pub status: u16,
    // pub content_type: String,
    pub body: String,
}

impl From<ProxyRequest> for ProxyRequestInternal {
    fn from(internal: ProxyRequest) -> Self {
        let mut hasher = std::collections::hash_map::DefaultHasher::new();
        internal.method.hash(&mut hasher);
        internal.resource.hash(&mut hasher);
        internal.body.hash(&mut hasher);
        let hash = hasher.finish().to_string();

        Self {
            method: internal.method,
            resource: internal.resource,
            body: internal.body,
            hash: hash,
        }
    }
}

// message send from client actor to server actor about received response
#[derive(Message)]
#[rtype(result = "()")]
pub struct ClientResponseMessage {
    pub client_id: Uuid,
    pub msg: Result<String, String>,
}
