use std::collections::HashMap;
use actix_web::http::StatusCode;
use actix_web::http::header::ContentType;
use actix_web::{get, post, web, Responder, HttpResponse};
use chrono::NaiveDateTime;
use sea_orm::{entity::*, DatabaseConnection, QueryOrder};
use serde::{Deserialize, Serialize};
use entity::user::{Entity as User, self};
use entity::role::{Entity as Role, self};
use uuid::Uuid;
use bcrypt::{self, hash, DEFAULT_COST};

#[derive(Serialize)]
struct AdminUserData {
    id: Uuid,
    role: String,
    email: String,
    name: String,
    created: NaiveDateTime,
    last_login: Option<NaiveDateTime>,
    data: Option<serde_json::value::Value>,
}

#[get("/admin-user-list")]
async fn admin_user_list(db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();
    let mut roles = HashMap::new();
    let role_models: Vec<role::Model> = Role::find().order_by_asc(role::Column::Name).all(db).await?;
    for role in role_models {
        roles.insert(role.id, role.name);
    }
    let user_models: Vec<user::Model> = User::find().order_by_asc(user::Column::Email).all(db).await?;
    let mut results = vec![];
    for user in user_models {
        results.push(AdminUserData{
            id: user.id,
            role: roles.get(&user.role_id).unwrap_or(&"Unknown".to_string()).to_owned(),
            email: user.email,
            name: user.name,
            created: user.created,
            last_login: user.last_login,
            data: user.data,
        });
    }
    Ok(web::Json(results))
}

#[get("/admin-user/{id}")]
async fn admin_user_single(user_id: web::Path<UserId>, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();
    let mut roles = HashMap::new();
    let role_models: Vec<role::Model> = Role::find().order_by_asc(role::Column::Name).all(db).await?;
    for role in role_models {
        roles.insert(role.id, role.name);
    }
    let maybe_user_model: Option<user::Model> = User::find_by_id(user_id.id).one(db).await?;
    match maybe_user_model {
        Some(user) => {
            Ok(HttpResponse::Ok().json(AdminUserData{
                id: user.id,
                role: roles.get(&user.role_id).unwrap_or(&"Unknown".to_string()).to_owned(),
                email: user.email,
                name: user.name,
                created: user.created,
                last_login: user.last_login,
                data: user.data,
            }))
        },
        None => Ok(HttpResponse::NotFound().body("User not found")),
    }
}

#[derive(Deserialize)]
pub struct AdminUserSet {
    id: Option<Uuid>,
    role: String,
    email: String,
    name: String,
    password: Option<String>,
    data: Option<serde_json::value::Value>,
}

#[post("/admin-user-set")]
async fn admin_user_set(mut user_set: web::Json<AdminUserSet>, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();
    if user_set.id.is_none() {
        user_set.id = Some(Uuid::new_v4());
    }
    let mut roles = HashMap::new();
    let mut roles_by_name = HashMap::new();
    let role_models: Vec<role::Model> = Role::find().order_by_asc(role::Column::Name).all(db).await?;
    for role in role_models {
        roles.insert(role.id, role.name.clone());
        roles_by_name.insert(role.name, role.id);
    }

    let user: user::Model = match User::find_by_id(user_set.id.unwrap()).one(db).await? {
        Some(row) => {
            let mut active_row: user::ActiveModel = row.into();
            active_row.role_id = Set(roles_by_name.get(&user_set.role).unwrap_or(&2).to_owned());
            active_row.email = Set(user_set.email.to_owned());
            active_row.name = Set(user_set.name.to_owned());
            if let Some(password) = user_set.password.to_owned() {
                active_row.password = Set(Some(hash(password, DEFAULT_COST)?));
            }
            active_row.data = Set(user_set.data.to_owned());
            active_row.update(db).await?
        },
        None => {
            let active_row = user::ActiveModel {
                id: ActiveValue::Set(user_set.id.unwrap()),
                role_id: Set(roles_by_name.get(&user_set.role).unwrap_or(&2).to_owned()),
                email: ActiveValue::Set(user_set.email.to_owned()),
                name: ActiveValue::Set(user_set.name.to_owned()),
                created: ActiveValue::NotSet,
                last_login: ActiveValue::NotSet,
                password: ActiveValue::Set(match user_set.password.to_owned() {
                    Some(password) => Some(hash(password, DEFAULT_COST)?),
                    None => None,
                }),
                data: ActiveValue::Set(user_set.data.to_owned()),
            };
            active_row.insert(db).await?
        },
    };
    Ok(HttpResponse::Ok().json(AdminUserData{
        id: user.id,
        role: roles.get(&user.role_id).unwrap_or(&"Unknown".to_string()).to_owned(),
        email: user.email,
        name: user.name,
        created: user.created,
        last_login: user.last_login,
        data: user.data,
    }))
}

#[derive(Serialize, Deserialize)]
pub struct UserId {
    id: Uuid,
}

#[post("/admin-user-remove")]
pub async fn admin_user_remove(user_id: web::Json<UserId>, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.as_ref();
    let user: user::Model = match User::find_by_id(user_id.id).one(db).await.unwrap_or(None) {
        Some(row) => row,
        None => return Ok(HttpResponse::build(StatusCode::NOT_FOUND)
            .insert_header(ContentType::html())
            .body("Not found")),
    };
    let mut roles = HashMap::new();
    let role_models: Vec<role::Model> = Role::find().order_by_asc(role::Column::Name).all(db).await?;
    for role in role_models {
        roles.insert(role.id, role.name);
    }
    let user_data = AdminUserData {
        id: user.id.clone(),
        role: roles.get(&user.role_id).unwrap_or(&"Unknown".to_string()).to_owned(),
        email: user.email.clone(),
        name: user.name.clone(),
        created: user.created.clone(),
        last_login: user.last_login.clone(),
        data: user.data.clone(),
    };

    user.delete(db).await?;

    Ok(HttpResponse::Ok().json(user_data))
}