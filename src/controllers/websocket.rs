use crate::websocket::{WsConn, WsServer, Disconnect};
use actix::Addr;
use actix_web::http::StatusCode;
use actix_web::http::header::ContentType;
use actix_web::web::Json;
use actix_web::{get, post, web::{Query, Data}, web::Payload, Error, HttpResponse, HttpRequest};
use actix_web_actors::ws;
use chrono::{Duration, Utc};
use jwt_compact::{prelude::*, alg::{Hs256, Hs256Key}};
use sea_orm::DatabaseConnection;
use uuid::Uuid;
use serde::{Serialize, Deserialize};
use derive_more::{Display, Error};
use actix_web::{error, Result};
use tokio::sync::Mutex;
use entity::concentrator::{Entity as Concentrator, self};
use sea_orm::EntityTrait;

// const MAX_FRAME_SIZE: usize = 65_536; // 64 KiB (default)
// the WS client has max frame size = 16 MiB and max message size 64 MiB: https://docs.rs/tungstenite/latest/tungstenite/protocol/struct.WebSocketConfig.html
// frontend request should fit into 16 MiB
const MAX_FRAME_SIZE: usize = 134_217_728; // 128 MiB (should be enough for retrieving readings)

#[derive(Debug, Display, Error)]
enum MyError {
    #[display(fmt = "Invalid token")]
    InvalidToken,

    #[display(fmt = "internal error")]
    InternalError,

    #[display(fmt = "bad request")]
    BadClientData,

    #[display(fmt = "bad request")]
    NotFound,

    // #[display(fmt = "timeout")]
    // Timeout,
}


impl error::ResponseError for MyError {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code())
            .insert_header(ContentType::html())
            .body(self.to_string())
    }

    fn status_code(&self) -> StatusCode {
        match *self {
            MyError::InternalError => StatusCode::INTERNAL_SERVER_ERROR,
            MyError::InvalidToken => StatusCode::BAD_REQUEST,
            MyError::BadClientData => StatusCode::BAD_REQUEST,
            MyError::NotFound => StatusCode::NOT_FOUND,
            // MyError::Timeout => StatusCode::GATEWAY_TIMEOUT,
        }
    }
}

const KEY_ALG_TYPE: &str = "HS256";

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct WebsocketTokenClaims {
    #[serde(rename = "sub")]
    subject: String,
    concentrator_id: Uuid,
}

#[derive(Serialize, Deserialize)]
pub struct WsTokenRequest {
    concentrator_id: Uuid,
    private_id: Uuid,
    // timestamp: NaiveDateTime,
}

#[derive(Serialize)]
struct WsTokenResponse {
    token: String,
}

#[post("/ws-token/")]
pub async fn websocket_token(
    req: Json<WsTokenRequest>,
    db: Data<DatabaseConnection>,
    key: Data<Hs256Key>
) -> Result<HttpResponse, Error> {
    let db = db.as_ref();
    let concentrator: concentrator::Model = match Concentrator::find_by_id(req.concentrator_id).one(db).await.unwrap_or(None)
    {
        Some(conc) => conc,
        None => return Err(MyError::BadClientData.into()),
    };

    // check private
    if !concentrator.private_id.eq(&req.private_id) {
        return Err(MyError::BadClientData.into());
    }

    let websocket_token_claims = WebsocketTokenClaims {
        subject: "/ws/".into(),
        concentrator_id: concentrator.id,
    };

    // Choose time-related options for token creation / validation.
    let time_options = TimeOptions::default();

    // create claims valid for 5 minutes
    let header:Header = Header::default().with_key_id(KEY_ALG_TYPE);
    let claims = Claims::new(websocket_token_claims)
        .set_duration_and_issuance(&time_options, Duration::minutes(5))
        .set_not_before(Utc::now());
    let token = match Hs256.token(&header, &claims, &key) {
        Ok(tok) => tok,
        Err(_) => return Err(MyError::InternalError.into()),
    };
    // let claims = Claims::with_custom_claims(websocket_token_claims, Duration::from_mins(5));
    // let token = match key.authenticate(claims) {
    //     Ok(tok) => tok,
    //     Err(_) => return Err(MyError::InternalError.into()),
    // };

    Ok(HttpResponse::Ok().json(WsTokenResponse {
        token: token,
    })) 
}

#[derive(Deserialize, Debug)]
pub struct WsQueryParams {
    token: String,
}

#[get("/ws/")]
pub async fn websocket_index(
    req: HttpRequest,
    stream: Payload,
    query_params: Query<WsQueryParams>,
    srv: Data<Mutex<Addr<WsServer>>>,
    db: Data<DatabaseConnection>,
    key: Data<Hs256Key>
) -> Result<HttpResponse, Error> {
    let db = db.as_ref();

    // Parse the token.
    let token = match UntrustedToken::new(&query_params.token) {
        Ok(tok) => tok,
        Err(_) => return Err(MyError::InvalidToken.into()),
    };

    // Before verifying the token, we might find the key which has signed the token
    // using the `Header.key_id` field.
    if !token.header().key_id.as_deref().eq(&Some(KEY_ALG_TYPE)) {
        return Err(MyError::InvalidToken.into());
    }

    // Validate the token integrity.
    let token: Token<WebsocketTokenClaims> = match Hs256.validator(&key).validate(&token) {
        Ok(tok) => tok,
        Err(_) => return Err(MyError::InvalidToken.into()),
    };

    // Validate additional conditions.
    let time_options = TimeOptions::default();
    let claims = match token.claims().validate_expiration(&time_options) {
        Ok(claims) => claims,
        Err(_) => return Err(MyError::InvalidToken.into()),
    };
    match claims.validate_maturity(&time_options) {
        Ok(_) => {},
        Err(_) => return Err(MyError::InvalidToken.into()),
    };

    let concentrator_id = claims.custom.concentrator_id;

    let concentrator: concentrator::Model = match Concentrator::find_by_id(concentrator_id).one(db).await.unwrap_or(None) {
        Some(conc) => conc,
        None => return Err(MyError::NotFound.into()),
    };

    println!("WS incoming concentrator with id {}", concentrator.id);

    let ws_server = {
        let guard = srv.lock().await;
        guard.clone()
    };

    // disconnect any previous session of this WS client
    let _ = ws_server.send(Disconnect { id: concentrator.id.clone(), manual: true }).await;

    let ws = WsConn::new(
        concentrator.id,
        ws_server,
    );

    // let resp = ws::start(ws, &req, stream)?;

    let resp = ws::WsResponseBuilder::new(ws, &req, stream)
        .frame_size(MAX_FRAME_SIZE)
        .start()?;

    Ok(resp)
}