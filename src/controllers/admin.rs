use actix_web::{get, Responder};
use crate::AuthUser;

#[get("/admin")]
async fn admin_index(user: AuthUser,) -> impl Responder {
    format!("You are admin [{}], you must be important!", user.id)
}