use actix_web::{get, post, web, Responder, HttpResponse};
use sea_orm::{entity::*, DatabaseConnection, QueryFilter, QueryOrder};
use serde::Deserialize;
use entity::concentrator::{Entity as Concentrator, self};
use entity::user_concentrator::{Entity as UserConcentrator, self};
use uuid::Uuid;
use crate::AuthUser;
use sumon_common::{BranchData, BranchState, BranchUpdateRequest};

#[get("/user-branch-list")]
async fn user_branch_list(user: AuthUser, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();
    let relations_with_branches: Vec<(user_concentrator::Model, Option<concentrator::Model>)> = UserConcentrator::find()
        .filter(user_concentrator::Column::UserId.eq(user.id))
        .find_also_related(Concentrator)
        .order_by_asc(user_concentrator::Column::Position)
        .all(db).await?;

    let mut results = vec![];
    for (relation, concentrator) in relations_with_branches {
        let concentrator = concentrator.unwrap();
        results.push(BranchData {
            id: concentrator.id,
            name: concentrator.name,
            state: serde_json::from_value(serde_json::value::Value::String(concentrator.state.to_value())).unwrap_or(BranchState::New),
            created: concentrator.created,
            last_connection: concentrator.last_connection,
            requests_count: concentrator.requests_count,
            data: concentrator.data,
            is_owner: relation.is_owner,
            position: relation.position,
            relation_created: relation.created,
        });
    }

    Ok(web::Json(results))
}

#[derive(Deserialize)]
struct BranchId {
    id: Uuid,
}

#[get("/user-branch/{id}")]
async fn user_branch(branch_id: web::Path<BranchId>, user: AuthUser, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();
    let maybe_relation_with_branch: Option<(user_concentrator::Model, Option<concentrator::Model>)> = UserConcentrator::find()
        .filter(user_concentrator::Column::ConcentratorId.eq(branch_id.id).and(user_concentrator::Column::UserId.eq(user.id)))
        .find_also_related(Concentrator)
        .order_by_asc(user_concentrator::Column::Position)
        .one(db).await?;

    if let Some(relation_with_branch) = maybe_relation_with_branch {
        let (relation, concentrator) = relation_with_branch;
        let concentrator = concentrator.unwrap();
        return Ok(HttpResponse::Ok().json(BranchData {
            id: concentrator.id,
            name: concentrator.name,
            state: serde_json::from_value(serde_json::value::Value::String(concentrator.state.to_value())).unwrap_or(BranchState::New),
            created: concentrator.created,
            last_connection: concentrator.last_connection,
            requests_count: concentrator.requests_count,
            data: concentrator.data,
            is_owner: relation.is_owner,
            position: relation.position,
            relation_created: relation.created,
        }))
    }
    Ok(HttpResponse::NotFound().body("Branch not found"))
}

#[post("/user-branch-update")]
async fn user_branch_update(branch_update: web::Json<BranchUpdateRequest>, user: AuthUser, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();

    let mut relation: user_concentrator::ActiveModel = match UserConcentrator::find_by_id((user.id, branch_update.id)).one(db).await? {
        Some(row) => row.into(),
        None => return Ok(HttpResponse::NotFound().body("Couldn't find specified user_concentrator relation.")),
    };

    relation.position = Set(branch_update.position.to_owned());
    let relation: user_concentrator::Model = relation.update(db).await?;


    let mut concentrator_active: concentrator::ActiveModel = match relation.find_related(Concentrator).one(db).await? {
        Some(row) => row.into(),
        None => return Ok(HttpResponse::NotFound().body("Couldn't find related concentrator.")),
    };

    // allow updating concentrator only for Superuser or owner of this specific concentrator
    let concentrator: concentrator::Model = if user.role.eq(&crate::AuthRole::Superuser) || relation.is_owner {
        concentrator_active.name = Set(branch_update.name.to_owned());
        concentrator_active.data = Set(branch_update.data.to_owned());
        concentrator_active.update(db).await?
    } else {
        concentrator_active.try_into_model()?
    };


    let branch_data = BranchData {
        id: concentrator.id,
        name: concentrator.name,
        state: serde_json::from_value(serde_json::value::Value::String(concentrator.state.to_value())).unwrap_or(BranchState::New),
        created: concentrator.created,
        last_connection: concentrator.last_connection,
        requests_count: concentrator.requests_count,
        data: concentrator.data,
        is_owner: relation.is_owner,
        position: relation.position,
        relation_created: relation.created,
    };

    Ok(HttpResponse::Ok().json(branch_data))
}