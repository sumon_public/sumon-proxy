use std::collections::HashSet;

use actix_web::http::StatusCode;
use actix_web::http::header::ContentType;
use actix_web::{get, post, web, Responder, HttpResponse};
use chrono::NaiveDateTime;
use sea_orm::{entity::*, DatabaseConnection, QueryFilter, QueryOrder, LoaderTrait, QuerySelect};
use serde::{Deserialize, Serialize};
use entity::concentrator::{Entity as Concentrator, self, ConcentratorState};
use entity::user_concentrator::{Entity as UserConcentrator, self};
use entity::user::{Entity as User, self};
use uuid::Uuid;

#[derive(Serialize)]
struct AdminBranchData {
    id: Uuid,
    name: String,
    state: String,
    created: NaiveDateTime,
    last_connection: Option<NaiveDateTime>,
    requests_count: i64,
    private_id: Uuid,
    data: Option<serde_json::value::Value>,
    owner_emails: Vec<String>,
}

#[get("/admin-branch-list")]
async fn admin_branch_list(db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();

    let concentrators: Vec<concentrator::Model> = Concentrator::find().order_by_asc(concentrator::Column::Name).all(db).await?;
    let users: Vec<Vec<user::Model>> = concentrators.load_many_to_many(User, UserConcentrator, db).await?;

    let mut results = vec![];

    for (concentrator, related_users) in concentrators.iter().zip(users.iter()) {
        let mut owner_emails = vec![];
        for user in related_users {
            // filter owners only?
            owner_emails.push(user.email.clone());
        }
        results.push(AdminBranchData {
            id: concentrator.id,
            name: concentrator.name.clone(),
            state: concentrator.state.to_value(),
            created: concentrator.created,
            last_connection: concentrator.last_connection,
            requests_count: concentrator.requests_count,
            private_id: concentrator.private_id,
            data: concentrator.data.clone(),
            owner_emails: owner_emails,
        });
    }

    Ok(web::Json(results))
}

#[derive(Deserialize)]
pub struct AdminBranchSet {
    id: Option<Uuid>,
    name: String,
    data: Option<serde_json::value::Value>,
    private_id: Option<Uuid>,
    owner_emails: Vec<String>,
}

#[get("/admin-branch/{id}")]
async fn admin_branch_single(concentrator_id: web::Path<ConcentratorId>, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();
    let concentrator: Option<concentrator::Model> = Concentrator::find_by_id(concentrator_id.id).one(db).await?;
    if concentrator.is_none() {
        return Ok(HttpResponse::NotFound().body("Branch not found"));
    }
    let concentrator = concentrator.unwrap();
    let related_users: Vec<user::Model> = concentrator.find_related(User).order_by_asc(user::Column::Email).all(db).await?;

    let mut owner_emails = vec![];
    for user in related_users {
        // filter owners only?
        owner_emails.push(user.email.clone());
    }
    Ok(HttpResponse::Ok().json(AdminBranchData {
        id: concentrator.id,
        name: concentrator.name.clone(),
        state: concentrator.state.to_value(),
        created: concentrator.created,
        last_connection: concentrator.last_connection,
        requests_count: concentrator.requests_count,
        private_id: concentrator.private_id,
        data: concentrator.data.clone(),
        owner_emails: owner_emails,
    }))
}

#[post("/admin-branch-set")]
async fn admin_branch_set(mut branch_set: web::Json<AdminBranchSet>, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();

    if branch_set.id.is_none() {
        branch_set.id = Some(Uuid::new_v4());
    }

    let concentrator: concentrator::Model = match Concentrator::find_by_id(branch_set.id.unwrap()).one(db).await? {
        Some(row) => {
            let mut active_row: concentrator::ActiveModel = row.into();
            active_row.name = Set(branch_set.name.to_owned());
            active_row.data = Set(branch_set.data.to_owned());
            if branch_set.private_id.is_some() {
                active_row.private_id = Set(branch_set.private_id.unwrap());
            }
            active_row.update(db).await?
        },
        None => {
            let active_row = concentrator::ActiveModel {
                id: ActiveValue::Set(branch_set.id.unwrap()),
                name: ActiveValue::Set(branch_set.name.to_owned()),
                state: ActiveValue::Set(ConcentratorState::New),
                created: ActiveValue::NotSet,
                last_connection: ActiveValue::NotSet,
                requests_count: ActiveValue::NotSet,
                private_id: ActiveValue::Set(match branch_set.private_id {
                    Some(private_id) => private_id,
                    None => Uuid::new_v4(),
                }),
                data: ActiveValue::Set(branch_set.data.to_owned()),
            };
            active_row.insert(db).await?
        },
    };

    let mut new_user_ids: Vec<Uuid> = user::Entity::find()
        .select_only()
        .column(user::Column::Id)
        .filter(user::Column::Email.is_in(branch_set.owner_emails.to_owned()))
        .into_tuple()
        .all(db).await?;
    let new_user_ids_2 = new_user_ids.clone();
    let new_user_ids_set: HashSet<&Uuid> = new_user_ids_2.iter().collect();

    let mut old_user_ids: Vec<Uuid> = user_concentrator::Entity::find()
        .select_only()
        .column(user_concentrator::Column::UserId)
        .filter(user_concentrator::Column::ConcentratorId.eq(concentrator.id))
        .into_tuple()
        .all(db).await?;
    let old_user_ids_set: HashSet<&Uuid> = old_user_ids.iter().collect();

    new_user_ids.retain(|uuid| !old_user_ids_set.contains(uuid));
    old_user_ids.retain(|uuid| !new_user_ids_set.contains(uuid));
    let missing_user_ids = new_user_ids;
    let redundant_user_ids = old_user_ids;

    let mut new_relations = vec![];
    for user_id in missing_user_ids {
        new_relations.push(user_concentrator::ActiveModel {
            user_id: ActiveValue::Set(user_id),
            concentrator_id: ActiveValue::Set(concentrator.id),
            is_owner: ActiveValue::NotSet,
            position: ActiveValue::NotSet,
            created: ActiveValue::NotSet,
        });
    }
    if new_relations.len() > 0 {
        user_concentrator::Entity::insert_many(new_relations)
            // .on_conflict(
            //     // on conflict do nothing
            //     OnConflict::column(user_concentrator::Column::UserId)
            //         .do_nothing()
            //         .to_owned()
            // )
            .exec(db).await?;
    }
    if redundant_user_ids.len() > 0 {
        user_concentrator::Entity::delete_many()
            .filter(
                user_concentrator::Column::ConcentratorId.eq(concentrator.id)
                .and(user_concentrator::Column::UserId.is_in(redundant_user_ids))
            )
            .exec(db)
            .await?;
    }

    let final_user_ids: Vec<Uuid> = user_concentrator::Entity::find()
        .select_only()
        .column(user_concentrator::Column::UserId)
        .filter(user_concentrator::Column::ConcentratorId.eq(concentrator.id))
        .into_tuple()
        .all(db).await?;

    let final_user_emails: Vec<String> = user::Entity::find()
        .select_only()
        .column(user::Column::Email)
        .filter(user::Column::Id.is_in(final_user_ids))
        .order_by_asc(user::Column::Email)
        .into_tuple()
        .all(db).await?;

    let branch_data = AdminBranchData {
        id: concentrator.id,
        name: concentrator.name,
        state: concentrator.state.to_value(),
        created: concentrator.created,
        last_connection: concentrator.last_connection,
        requests_count: concentrator.requests_count,
        private_id: concentrator.private_id,
        data: concentrator.data,
        owner_emails: final_user_emails,
    };

    Ok(HttpResponse::Ok().json(branch_data))
}

#[derive(Serialize, Deserialize)]
pub struct ConcentratorId {
    id: Uuid,
}

#[post("/admin-branch-remove")]
pub async fn admin_branch_remove(concentrator_id: web::Json<ConcentratorId>, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.as_ref();
    let concentrator: concentrator::Model = match Concentrator::find_by_id(concentrator_id.id).one(db).await.unwrap_or(None) {
        Some(row) => row,
        None => return Ok(HttpResponse::build(StatusCode::NOT_FOUND)
            .insert_header(ContentType::html())
            .body("Not found")),
    };
    let user_ids: Vec<Uuid> = user_concentrator::Entity::find()
        .select_only()
        .column(user_concentrator::Column::UserId)
        .filter(user_concentrator::Column::ConcentratorId.eq(concentrator.id))
        .into_tuple()
        .all(db).await?;
    let user_emails: Vec<String> = user::Entity::find()
        .select_only()
        .column(user::Column::Email)
        .filter(user::Column::Id.is_in(user_ids))
        .order_by_asc(user::Column::Email)
        .into_tuple()
        .all(db).await?;
    let branch_data = AdminBranchData {
        id: concentrator.id.clone(),
        name: concentrator.name.clone(),
        state: concentrator.state.clone().to_value(),
        created: concentrator.created.clone(),
        last_connection: concentrator.last_connection.clone(),
        requests_count: concentrator.requests_count.clone(),
        private_id: concentrator.private_id.clone(),
        data: concentrator.data.clone(),
        owner_emails: user_emails,
    };

    concentrator.delete(db).await?;

    Ok(HttpResponse::Ok().json(branch_data))
}