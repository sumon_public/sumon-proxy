use actix_web::{get, post, web, HttpResponse, http::{StatusCode, header::ContentType}, Responder};
use sea_orm::{entity::*, DatabaseConnection};
use serde::Deserialize;
use entity::user::{Entity as User, self};
use entity::role::{Entity as Role, self};
use sumon_common::UserData;
use uuid::Uuid;
use crate::AuthUser;

pub async fn get_user_response(id: Uuid, db: &DatabaseConnection) -> Option<UserData> {
    let user_row: user::Model = match User::find_by_id(id).one(db).await.unwrap_or(None) {
        Some(row) => row,
        None => return None,
    };
    let role: role::Model = match user_row.find_related(Role).one(db).await.unwrap_or(None) {
        Some(row) => row,
        None => return None,
    };

    Some(UserData {
        id: user_row.id,
        role: serde_json::from_value(serde_json::value::Value::String(role.name)).unwrap_or(sumon_common::AuthRole::Owner),
        email: user_row.email,
        name: user_row.name,
        created: user_row.created,
        last_login: user_row.last_login,
        data: match user_row.data {
            Some(data) => serde_json::from_value(data).ok(),
            None => None,
        },
    })
}

#[get("/user")]
async fn user_info(user: AuthUser, db: web::Data<DatabaseConnection>,) -> impl Responder {
    let db = db.as_ref();
    match get_user_response(user.id, db).await {
        Some(user_response) => HttpResponse::Ok().json(user_response),
        None => HttpResponse::build(StatusCode::NOT_FOUND).insert_header(ContentType::html()).body("Not found"),
    }
}

#[derive(Deserialize)]
pub struct UserId {
    id: Uuid,
}

#[post("/user-remove")]
pub async fn user_remove(user_id: web::Json<UserId>, user: AuthUser, db: web::Data<DatabaseConnection>,) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.as_ref();
    if !user_id.id.eq(&user.id) {
        return Ok(HttpResponse::Forbidden().json("{\"msg\":\"Removing other users is not allowed!\"}"));
    }
    let user_model: user::Model = match User::find_by_id(user_id.id).one(db).await.unwrap_or(None) {
        Some(row) => row,
        None => return Ok(HttpResponse::build(StatusCode::NOT_FOUND)
            .insert_header(ContentType::html())
            .body("Not found")),
    };
    let user_response = get_user_response(user_model.id, db).await;

    user_model.delete(db).await?;

    Ok(match user_response {
        Some(user_response) => HttpResponse::Ok().json(user_response),
        None => HttpResponse::build(StatusCode::NOT_FOUND).insert_header(ContentType::html()).body("Not found"),
    })
}