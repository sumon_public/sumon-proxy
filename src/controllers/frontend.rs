use actix_web::Result;
use actix_files::NamedFile;
use std::path::PathBuf;

pub async fn frontend_index() -> Result<NamedFile> {
    let path: PathBuf = "./frontend/dist/index.html".parse().unwrap();
    Ok(NamedFile::open(path)?)
}