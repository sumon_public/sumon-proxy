use std::{collections::HashMap, error::Error, fmt};

use actix_jwt_auth_middleware::{TokenSigner, AuthResult, AuthError};
use actix_web::{cookie::{Cookie, SameSite}, get, http::{header::ContentType, StatusCode}, post, web, HttpResponse, Responder};
use bcrypt::{verify, hash, DEFAULT_COST};
use chrono::Utc;
use jwt_compact::alg::Hs256;
use regex::Regex;
use serde::{Serialize, Deserialize};
use uuid::Uuid;
use crate::{AuthUser, AuthRole, AppState};
use entity::{user::{Entity as User, self}, oauth::OauthType};
use entity::role::{Entity as Role, self};
use entity::oauth::{Entity as Oauth, self};
use sea_orm::{entity::*, query::*, DatabaseConnection};
use google_oauth::AsyncClient;
use crate::controllers::get_user_response;
use sumon_common::UserData;

#[derive(Serialize, Deserialize)]
pub struct LoginRequest {
    email: String,
    password: String,
}

#[post("/login")]
async fn login_index(
    req: web::Json<LoginRequest>,
    token_signer: web::Data<TokenSigner<AuthUser, Hs256>>,
    db: web::Data<DatabaseConnection>,
) -> AuthResult<HttpResponse> {
    let db = db.as_ref();
    let user: user::Model = match User::find().filter(user::Column::Email.eq(&req.email)).one(db).await.unwrap_or(None) {
        Some(row) => row,
        None => return Ok(HttpResponse::build(StatusCode::UNAUTHORIZED)
            .insert_header(ContentType::html())
            .body("Invalid credentials")),
    };
    let role: role::Model = match user.find_related(Role).one(db).await.unwrap_or(None) {
        Some(row) => row,
        None => return Ok(HttpResponse::build(StatusCode::UNAUTHORIZED)
        .insert_header(ContentType::html())
        .body("Invalid credentials")),
    };

    if user.password.is_none() {
        return Ok(HttpResponse::build(StatusCode::UNAUTHORIZED)
            .insert_header(ContentType::html())
            .body("Invalid credentials"));
    };

    // match verify(&req.password, &user.password.unwrap()) {
    let password_hash = user.password.clone().unwrap();
    match verify(&req.password, &password_hash) {
        Ok(valid) => {
            if valid {
                // update user row
                let mut user: user::ActiveModel = user.into();
                user.last_login = Set(Some(Utc::now().naive_utc()));
                let user = match user.update(db).await {
                    Ok(user) => user,
                    Err(_) => return Ok(HttpResponse::build(StatusCode::UNAUTHORIZED)
                        .insert_header(ContentType::html())
                        .body("Error fetching user")),
                };

                let user = AuthUser {
                    id: user.id,
                    role: match role.name.as_str() {
                        "Owner" => AuthRole::Owner,
                        "Superuser" => AuthRole::Superuser,
                        _ => AuthRole::Owner,
                    }
                };

                let access_cookie = token_signer.create_access_cookie(&user)?;
                let refresh_cookie = token_signer.create_refresh_cookie(&user)?;

                return Ok(HttpResponse::Ok()
                    .cookie(access_cookie)
                    .cookie(refresh_cookie)
                    .body("You are now logged in"));
            }
        },
        Err(_) => {},
    }
    Ok(HttpResponse::build(StatusCode::UNAUTHORIZED)
        .insert_header(ContentType::html())
        .body("Invalid credentials"))
}

#[get("/login")]
async fn login_get(
    req: web::Query<LoginRequest>,
    token_signer: web::Data<TokenSigner<AuthUser, Hs256>>,
    db: web::Data<DatabaseConnection>,
) -> AuthResult<HttpResponse> {
    let db = db.as_ref();
    let user: user::Model = match User::find().filter(user::Column::Email.eq(&req.email)).one(db).await.unwrap_or(None) {
        Some(row) => row,
        None => return Ok(HttpResponse::build(StatusCode::UNAUTHORIZED)
            .insert_header(ContentType::html())
            .body("Invalid credentials")),
    };
    let role: role::Model = match user.find_related(Role).one(db).await.unwrap_or(None) {
        Some(row) => row,
        None => return Ok(HttpResponse::build(StatusCode::UNAUTHORIZED)
        .insert_header(ContentType::html())
        .body("Invalid credentials")),
    };

    if user.password.is_none() {
        return Ok(HttpResponse::build(StatusCode::UNAUTHORIZED)
            .insert_header(ContentType::html())
            .body("Invalid credentials"));
    };

    // match verify(&req.password, &user.password.unwrap()) {
    let password_hash = user.password.clone().unwrap();
    match verify(&req.password, &password_hash) {
        Ok(valid) => {
            if valid {
                // update user row
                let mut user: user::ActiveModel = user.into();
                user.last_login = Set(Some(Utc::now().naive_utc()));
                let user = match user.update(db).await {
                    Ok(user) => user,
                    Err(_) => return Ok(HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR)
                        .insert_header(ContentType::html())
                        .body("Error fetching user")),
                };

                let user = AuthUser {
                    id: user.id,
                    role: match role.name.as_str() {
                        "Owner" => AuthRole::Owner,
                        "Superuser" => AuthRole::Superuser,
                        _ => AuthRole::Owner,
                    }
                };

                let access_cookie = token_signer.create_access_cookie(&user)?;
                let refresh_cookie = token_signer.create_refresh_cookie(&user)?;

                return Ok(HttpResponse::Ok()
                    .cookie(access_cookie)
                    .cookie(refresh_cookie)
                    .body("You are now logged in"));
            }
        },
        Err(_) => {},
    }
    Ok(HttpResponse::build(StatusCode::UNAUTHORIZED)
        .insert_header(ContentType::html())
        .body("Invalid credentials"))
}

#[get("/logout")]
pub async fn log_out() -> actix_web::HttpResponse {
    let mut response = actix_web::HttpResponse::Ok();
    let mut cookie = Cookie::build("access_token", "value").finish();
    cookie = unify_cookie(cookie);
    cookie.make_removal();
    // response.add_removal_cookie(cookie);
    response.cookie(cookie);

    let mut refresh_cookie = Cookie::build("refresh_token", "value").finish();
    refresh_cookie = unify_cookie(refresh_cookie);
    refresh_cookie.make_removal();
    response.cookie(refresh_cookie);

    response.body("")
}

fn validate_email<'de, D>(deserializer: D) -> Result<String, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let email_regex =
        Regex::new(r"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$").expect("Invalid regex");
    let email: String = serde::Deserialize::deserialize(deserializer)?;
    
    if email_regex.is_match(&email) {
        Ok(email)
    } else {
        Err(serde::de::Error::custom("Invalid email address"))
    }
}

#[derive(Deserialize)]
pub struct RegisterWithPasswordRequest {
    #[serde(deserialize_with = "validate_email")]
    email: String,
    name: String,
    password: String,
}

#[derive(Serialize)]
pub struct RegisterWithPasswordResponse {
    id: Uuid,
    role: String,
    email: String,
    name: String,
}

#[post("/register")]
pub async fn register_with_password(
    req: web::Json<RegisterWithPasswordRequest>,
    db: web::Data<DatabaseConnection>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.as_ref();
    let mut roles = HashMap::new();
    let mut roles_by_name = HashMap::new();
    let role_models: Vec<role::Model> = Role::find().order_by_asc(role::Column::Name).all(db).await?;
    for role in role_models {
        roles.insert(role.id, role.name.clone());
        roles_by_name.insert(role.name, role.id);
    }
    let active_row = user::ActiveModel {
        id: ActiveValue::Set(Uuid::new_v4()),
        role_id: Set(roles_by_name.get("Owner").unwrap_or(&2).to_owned()),
        email: ActiveValue::Set(req.email.to_owned()),
        name: ActiveValue::Set(req.name.to_owned()),
        created: ActiveValue::NotSet,
        last_login: ActiveValue::NotSet,
        password: ActiveValue::Set(Some(hash(req.password.to_owned(), DEFAULT_COST)?)),
        data: ActiveValue::NotSet,
    };
    let user: user::Model = match active_row.insert(db).await {
        Ok(res) => res,
        Err(db_err) => {
            if db_err.to_string().contains("duplicate key value violates unique constraint") {
                // return HTTP status 422 (Unprocessable Entity)
                return Ok(HttpResponse::Ok().status(StatusCode::UNPROCESSABLE_ENTITY)
                    .json("{\"msg\":\"Duplicate key value violates unique constraint\"}"));
            }
            Err(db_err)?
        }
    };
    Ok(HttpResponse::Ok().json(RegisterWithPasswordResponse{
        id: user.id,
        role: roles.get(&user.role_id).unwrap_or(&"Unknown".to_string()).to_owned(),
        email: user.email,
        name: user.name,
    }))
}

#[derive(Deserialize)]
pub struct LoginGoogleRequest {
    token: String,
}

#[derive(Serialize)]
pub struct LoginGoogleErrResponse {
    msg: String,
}

#[derive(Deserialize, Serialize)]
pub struct UserCustomData {
    google_data: Option<serde_json::value::Value>,
}

#[derive(Deserialize, Serialize)]
pub struct GoogleCustomData {
    picture: String,
    name: String,
    given_name: String,
    family_name: String,
}

#[post("/login-google")]
pub async fn login_google(
    req: web::Json<LoginGoogleRequest>,
    app: web::Data<AppState>,
    token_signer: web::Data<TokenSigner<AuthUser, Hs256>>,
    db: web::Data<DatabaseConnection>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.as_ref();
    let mut roles = HashMap::new();
    let mut roles_by_name = HashMap::new();
    let role_models: Vec<role::Model> = Role::find().order_by_asc(role::Column::Name).all(db).await?;
    for role in role_models {
        roles.insert(role.id, role.name.clone());
        roles_by_name.insert(role.name, role.id);
    }
  
    let client = AsyncClient::new(&app.google_client_id); // xxxxx.apps.googleusercontent.com
    
    let verification_result = client.validate_id_token(req.token.to_owned()).await;
    let google_data = match &verification_result {
        Ok(data) => data,
        Err(e) => return Ok(HttpResponse::Unauthorized().json(LoginGoogleErrResponse{msg: e.to_string()})),
    };

    // search for oauth row by client_id -> best path
    let oauth_and_new: (oauth::ActiveModel, bool) = match Oauth::find()
    .filter(oauth::Column::ClientId.eq(&google_data.sub).and(oauth::Column::Type.eq(OauthType::Google)))
    .one(db).await? {
        Some(row) => (row.into(), false),
        None => {
            // client_id not found, so try to pair by verified email
            if google_data.email_verified.is_some() && google_data.email_verified.unwrap_or(false) {
                let oauth_row_by_email: Option<oauth::Model>;
                let google_data_email = google_data.email.clone().unwrap_or("".to_string());
                let google_data_name = google_data.name.clone().unwrap_or("".to_string());

                // search for oauth row by verified google email and type=Google
                let user: user::Model = match Oauth::find()
                .filter(oauth::Column::Username.eq(&google_data_email).and(oauth::Column::Type.eq(OauthType::Google)))
                .one(db).await? {
                    Some(oauth_row) => {
                        oauth_row_by_email = Some(oauth_row.clone());
                        match User::find_by_id(oauth_row.user_id).one(db).await? {
                            Some(user_row) => user_row,
                            None => return Ok(HttpResponse::InternalServerError()
                                .json(LoginGoogleErrResponse{msg: "Found oauth entry by 3rd party username, but relevant user is missing!".to_string()})
                            )
                        }
                    },
                    None => {
                        // oauth_row not found, lets create a new user
                        oauth_row_by_email = None;
                        let user_active_row = user::ActiveModel {
                            id: ActiveValue::Set(Uuid::new_v4()),
                            role_id: Set(roles_by_name.get("Owner").unwrap_or(&2).to_owned()),
                            email: ActiveValue::Set(google_data_email.clone()),
                            name: ActiveValue::Set(if google_data_name.is_empty() {google_data_email.clone()} else {google_data_name.clone()}),
                            created: ActiveValue::NotSet,
                            last_login: ActiveValue::NotSet,
                            password: ActiveValue::NotSet,
                            data: ActiveValue::NotSet,
                        };
                        match user_active_row.insert(db).await {
                            Ok(res) => res,
                            Err(db_err) => {
                                if db_err.to_string().contains("duplicate key value violates unique constraint") {
                                    // return HTTP status 422 (Unprocessable Entity)
                                    return Ok(HttpResponse::Ok().status(StatusCode::UNPROCESSABLE_ENTITY)
                                        .json("{\"msg\":\"Duplicate key value violates unique constraint\"}"));
                                }
                                Err(db_err)?
                            }
                        }
                    },
                };

                let oauth_active_row = oauth::ActiveModel {
                    id: ActiveValue::Set(if oauth_row_by_email.is_some() {oauth_row_by_email.clone().unwrap().id} else {Uuid::new_v4()}),
                    user_id: ActiveValue::Set(user.id),
                    r#type: ActiveValue::Set(OauthType::Google),
                    username: ActiveValue::Set(Some(google_data_email.clone())),
                    client_id: ActiveValue::Set(Some(google_data.sub.clone())),
                    created: ActiveValue::NotSet,
                    last_login: ActiveValue::NotSet,
                    data: ActiveValue::NotSet,
                };
                (oauth_active_row, !oauth_row_by_email.is_some())
            } else {
                return Ok(HttpResponse::Unauthorized().json(LoginGoogleErrResponse{msg: "Email address provided by google is not verified!".to_string()}));
            }
        },
    };

    // prepare some custom additional data from google for saving
    let google_custom_data = match serde_json::to_value(GoogleCustomData {
        picture: google_data.picture.clone().unwrap_or("".to_string()).clone(),
        name: google_data.name.clone().unwrap_or("".to_string()),
        given_name: google_data.given_name.clone().unwrap_or("".to_string()).clone(),
        family_name: google_data.family_name.clone().unwrap_or("".to_string()).clone(),
    }) {
        Ok(serialized_data) => Some(serialized_data),
        Err(_) => None,
    };

    // save oauth row
    let (mut oauth_active_model, is_new) = oauth_and_new;
    oauth_active_model.last_login = Set(Some(Utc::now().naive_utc()));
    oauth_active_model.data = Set(google_custom_data);
    let oauth_row: oauth::Model = match is_new {
        true => oauth_active_model.insert(db).await?,
        false => oauth_active_model.update(db).await?,
    };
    
    // update user row
    let new_custom_data = match oauth_row.data {
        Some(data) => Some(serde_json::to_value(UserCustomData{google_data: Some(data)})?),
        None => None,
    };
    if let Some(user) = User::find_by_id(oauth_row.user_id).one(db).await? {
        let mut user: user::ActiveModel = user.into();
        user.last_login = Set(Some(Utc::now().naive_utc()));
        user.data = Set(new_custom_data);
        let _ = user.update(db).await?;
    }

    let response_data: UserData = match get_user_response(oauth_row.user_id, db).await {
        Some(user_response) => user_response,
        None => return Ok(HttpResponse::build(StatusCode::INTERNAL_SERVER_ERROR).insert_header(ContentType::html()).body("Unexpected not found")),
    };

    // create user identity
    let user = AuthUser {
        id: response_data.id,
        role: match response_data.role {
            sumon_common::AuthRole::Owner => AuthRole::Owner,
            sumon_common::AuthRole::Superuser => AuthRole::Superuser,
        }
    };

    let access_cookie = match token_signer.create_access_cookie(&user) {
        Ok(cook) => cook,
        Err(e) => return Err(Box::new(WrappedAuthError(e))),
    };
    let refresh_cookie = match token_signer.create_refresh_cookie(&user) {
        Ok(cook) => cook,
        Err(e) => return Err(Box::new(WrappedAuthError(e))),
    };

    Ok(
        HttpResponse::Ok()
            .cookie(access_cookie)
            .cookie(refresh_cookie)
            .json(response_data)
    )
}

/// Sets common path, http_only and same_site.
fn unify_cookie(mut cookie: Cookie) -> Cookie {
    cookie.set_secure(true);
    cookie.set_path("/"); // ensure the same path for the whole proxy app
    cookie.set_http_only(true); // js shouldn't be able to access this cookie
    cookie.set_same_site(SameSite::Strict); // requests originating from the same site that set the cookie
    cookie
}

// Custom wrapper error type that wraps AuthError
pub struct WrappedAuthError(pub AuthError);

impl fmt::Debug for WrappedAuthError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "WrappedAuthError({:?})", self.0)
    }
}

impl fmt::Display for WrappedAuthError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "WrappedAuthError: {}", self.0)
    }
}

impl Error for WrappedAuthError {}