use actix::Addr;
use actix_web::{post, web::{self, Bytes}, Result, HttpResponse, Responder, HttpRequest, http::{header::ContentType, StatusCode}};
use sea_orm::{DatabaseConnection, EntityTrait};
use entity::user_concentrator::{Entity as UserConcentrator, self};
use serde::Deserialize;
use tokio::sync::Mutex;
use uuid::Uuid;
use crate::{websocket::{WsServer, RequestMessage, ProxyRequest}, AuthUser};

#[derive(Deserialize)]
pub struct RequestPathParams {
    concentrator_id: Uuid,
    resource: String,
}

#[post("/proxy/{concentrator_id}/{resource}")]
pub async fn proxy_index(
    path_params: web::Path<RequestPathParams>,
    srv: web::Data<Mutex<Addr<WsServer>>>,
    req: HttpRequest,
    body_bytes: Bytes,
    user: AuthUser,
    db: web::Data<DatabaseConnection>,
) -> Result<impl Responder, Box<dyn std::error::Error>> {
    let db = db.get_ref();
    if !user.role.eq(&crate::AuthRole::Superuser) {
        let maybe_relation: Option<user_concentrator::Model> = UserConcentrator::find_by_id((user.id, path_params.concentrator_id)).one(db).await?;
        match maybe_relation {
            Some(user_concentrator) => {
                // relation exists, if user is not the owner deny set and remove actions
                if !user_concentrator.is_owner && (
                    path_params.resource.ends_with("set") || path_params.resource.ends_with("remove")
                ) {
                    return Ok(HttpResponse::NotFound().body("Authenticated user has insufficient privileges to set or remove object"));
                };
            },
            None => {             
                return Ok(HttpResponse::NotFound().body("Concentrator not found or the authenticated user has insufficient privileges"));
            },
        };
    }

    let ws_server = {
        let guard = srv.lock().await;
        guard.clone()
    };

    let method = req.method().as_str().to_owned();
    let body = String::from_utf8(body_bytes.to_vec()).unwrap_or("{}".to_string());

    // Create a RequestData struct from the extracted information
    let request_data = ProxyRequest {
        method,
        resource: path_params.resource.clone(),
        body,
    };

    let request = RequestMessage{
        client_id: path_params.concentrator_id,
        request: request_data,
    };

    // Wait for response from the WS client concentrator
    let result = ws_server.send(request).await?;

    match result {
        Ok(json_response) => {
            Ok(
                HttpResponse::Ok()
                    .status(StatusCode::from_u16(json_response.status).unwrap_or(StatusCode::INTERNAL_SERVER_ERROR))
                    .insert_header(ContentType::json())
                    .body(json_response.body)
            )
        }
        Err(err) => Ok(HttpResponse::InternalServerError().insert_header(ContentType::plaintext()).body(format!("Požadavek se nepodařilo zpracovat: {}", err))),
    }
}