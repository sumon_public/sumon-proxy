use actix_web::{get, Responder, HttpResponse};

#[get("/")]
pub async fn api_index() -> impl Responder {
    HttpResponse::Ok().body("Proxy HTTP server")
}