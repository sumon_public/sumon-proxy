mod controllers;
mod websocket;

use actix::prelude::*;
use actix_cors::Cors;
use actix_state_guards::UseStateGuardOnScope;
use actix_jwt_auth_middleware::{TokenSigner, FromRequest, Authority};
use actix_jwt_auth_middleware::use_jwt::UseJWTOnScope;
use actix_web::cookie::{Cookie, SameSite};
use actix_web::{web::{Data, self}, HttpServer, App, http::StatusCode, error::InternalError};
use actix_files as fs;
use jwt_compact::alg::{Hs256Key, Hs256};
use migration::{Migrator, MigratorTrait};
use serde::{Serialize, Deserialize};
use tokio::sync::Mutex;
use uuid::Uuid;
use std::time::Duration;
use sea_orm::{ConnectOptions, Database};
use dotenv::dotenv;
use base64::{Engine as _, engine::general_purpose};


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("proxy");

    dotenv().ok(); // This line loads the environment variables from the ".env" file.
    std::env::set_var("RUST_LOG", "actix_web=debug,actix_server=trace");
    env_logger::init();

    let mut db_opt = ConnectOptions::new(std::env::var("DATABASE_URL").expect("Error: DATABASE_URL not found").to_owned());
    db_opt.max_connections(50)
        .min_connections(0)
        .connect_timeout(Duration::from_secs(8))
        .acquire_timeout(Duration::from_secs(8))
        .idle_timeout(Duration::from_secs(8))
        .max_lifetime(Duration::from_secs(8))
        .sqlx_logging(true)
        // .sqlx_logging_level(log::LevelFilter::Info)
        .sqlx_logging_level(log::LevelFilter::Debug)
        .set_schema_search_path("public".into()); // Setting default PostgreSQL schema

    let db = Database::connect(db_opt).await.unwrap();
    Migrator::up(&db, None).await.unwrap();

    // Cloning Pool is cheap as it is simply a reference-counted handle to the inner pool state.
    // https://docs.rs/sqlx/0.5.13/sqlx/struct.Pool.html
    let db_data = Data::new(db.clone());

    let ws_server = websocket::WsServer::new(db.clone()).start(); //create and spin up a websocket server actor
    let ws_server_data = Data::new(Mutex::new(ws_server));

    let key_base64 = std::env::var("PRIVATE_KEY").expect("Error: PRIVATE_KEY not found");
    let key_bytes = general_purpose::STANDARD_NO_PAD.decode(key_base64)
        .expect("Error: PRIVATE_KEY could not be decoded!");
    let key = Hs256Key::new(key_bytes);
    let key_data = Data::new(key.clone());   

    let port: u16 = std::env::var("LISTEN_PORT").expect("Error: LISTEN_PORT not found").parse().unwrap();
    HttpServer::new(move || {
        let authority = Authority::<AuthUser, Hs256, _, _>::new()
            .refresh_authorizer(|| async move { Ok(()) })
            .token_signer(Some(
                TokenSigner::new()
                    .signing_key(key.clone())
                    .algorithm(Hs256)
                    .refresh_token_lifetime(Duration::from_secs(2*168*60*60)) // two weeks
                    .cookie_builder(Cookie::build("", "").secure(true).path("/").http_only(true).same_site(SameSite::Strict))
                    .build()
                    .expect("Error: Could not create a TokenSigner"),
            ))
            .verifying_key(key.clone())
            .build()
            .expect("");

        App::new()
            .app_data(db_data.clone())
            .app_data(Data::new(AppState {
                private_key: std::env::var("PRIVATE_KEY").expect("Error: PRIVATE_KEY not found"),
                google_client_id: std::env::var("GOOGLE_CLIENT_ID").expect("Error: GOOGLE_CLIENT_ID not found"),
            }))
            .app_data(ws_server_data.clone())
            .app_data(key_data.clone())
            .wrap(Cors::permissive())
            .service(web::scope("/_")
                .service(controllers::api_index)
                .service(controllers::login_index)
                .service(controllers::login_get)
                .service(controllers::log_out)
                .service(controllers::register_with_password)
                .service(controllers::login_google)
                .service(controllers::websocket_token)
                .service(controllers::websocket_index)
                .use_jwt(
                    authority,
                    web::scope("")
                        .service(controllers::user_info)
                        .service(controllers::user_remove)
                        .service(controllers::user_branch_list)
                        .service(controllers::user_branch)
                        .service(controllers::user_branch_update)
                        .service(controllers::proxy_index)
                    .service(web::scope("").use_state_guard(
                        |user: AuthUser| async move {
                            if user.role == AuthRole::Superuser {
                                Ok(())
                            } else {
                                Err(InternalError::new(
                                    "You are not an admin",
                                    StatusCode::UNAUTHORIZED,
                                ))
                            }
                        },
                        web::scope("")
                            .service(controllers::admin_index)
                            .service(controllers::admin_branch_list)
                            .service(controllers::admin_branch_single)
                            .service(controllers::admin_branch_set)
                            .service(controllers::admin_branch_remove)
                            .service(controllers::admin_user_list)
                            .service(controllers::admin_user_single)
                            .service(controllers::admin_user_set)
                            .service(controllers::admin_user_remove)
                    )),
                )
            )
            .route("/", web::get().to(controllers::frontend_index))
            .route("/", web::post().to(controllers::frontend_index))
            .route("/login", web::get().to(controllers::frontend_index))
            .route("/login", web::post().to(controllers::frontend_index))
            .route("/registration", web::get().to(controllers::frontend_index))
            .route("/registration", web::post().to(controllers::frontend_index))
            .route("/home", web::get().to(controllers::frontend_index))
            .route("/branch/{id}", web::get().to(controllers::frontend_index))
            .route("/branch/{id}", web::post().to(controllers::frontend_index))
            .service(fs::Files::new("/", "./frontend/dist").index_file("index.html"))
    })
    .bind((std::env::var("LISTEN_IP").expect("Error: LISTEN_IP not found"), port))?
    .run()
    .await
}

#[derive(Serialize, Deserialize, Debug, Clone, FromRequest)]
pub struct AuthUser {
    role: AuthRole,
    id: Uuid,
}

#[derive(Serialize, Deserialize, Debug, Clone, FromRequest, PartialEq)]
pub enum AuthRole {
    Owner,
    Superuser,
}

#[allow(dead_code)]
pub struct AppState {
    private_key: String,
    google_client_id: String,
}